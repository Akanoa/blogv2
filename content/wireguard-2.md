+++
title = "Le Réseau de zéro (partie 3)"
date = "2023-12-12"
draft = false
template  = 'post.html'

[taxonomies]
categories = ["Réseau"]
tags = ["linux", "sys admin", "réseau"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = true
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Mais qu'est ce que c'est un vlan?" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/wireguard-2.jpg" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Mais qu'est ce que c'est un vlan?" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/wireguard-2.jpg" },
    { property = "og:url", content="https://lafor.ge/wireguard-2" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++
{% detail(header="Les articles de la série") %}
{{ toc(except=2, parts=[0,1], prefix="wireguard")}}
{% end %}

Bonjour à toutes et à tous 😀

Oui, plus personne ne l'attendait même plus moi ^^' 

Voici venu le moment de la partie 3 qui sert de prologue à l'article sur wireguard.

Aujourd'hui nous allons nous poser une question:

{% question() %}
Comment isoler efficacement des domaines de diffusion ?
{% end %}


**Disclaimer**:

Le principe de VLAN est une technologie qui a eu son [implémentation](https://fr.wikipedia.org/wiki/Cisco_Inter-Switch_Link) avant sa [standardisation](https://fr.wikipedia.org/wiki/IEEE_802.1Q).

Ainsi plusieurs différences peuvent apparaître dans les mise en oeuvres des principes.

Cet article n'est pas une préparation au concours Cisco, je vais donc rester vague dans les implémentations et ne parler que des concepts sous-jacents.

Les switchs que je vais utiliser tout au long de l'article sont des switchs managés, qui ont la capacité de faire plus de choses que leur homologues non-managés, mais ça se ressent également sur le prix à l'achat. 😝 

Il existe également différent types de VLAN, cet article ne traitera que du VLAN de couche 2 associer à des ports.

Bien ces mises au point réalisées, on peut attaquer. ^^

## Problématique

Pour illustrer ceci prenons le réseau physique suivant:

{{ image(path="reseau-3/vlan-01.jpg", width="70%") }}

Il est composé de 4 machines et d'une [passerelle](/wireguard-0/#passerelle)

- Un serveur web qui distribue du contenu vers internet
- Une base de données pour ce même serveur web
- 2 postes de travail de comptabilité

Pour des raisons de confidentialité et de sécurité, nous souhaitons que les postes de comptabilité ne soient pas visibles depuis le serveur web
qui est une machine connectée à internet et donc qui peut être un point d'entré pour des attaques malveillantes.

On créer alors un `Réseau 1` qui contient le réseau accédant à internet (la BDD pourrait en être exclu). Ici en bleu.

{{ image(path="reseau-3/vlan-02.jpg", width="70%" alt="Le serveur web et la bdd sont isolés") }}

Puis nous avons un second `Réseau 2` qui contient exclusivement les ordinateurs du service comptabilité ainsi que la passerelle mais sans
accès internet(ils n'en ont apparemment pas besoin ^^').

{{ image(path="reseau-3/vlan-03.jpg", width="70%" alt="Le service comptabilité ne voient pas le serveur web et lui ne voit pas la comptabilité") }}

La solution est alors de créér deux réseaux avec deux switchs différenciés

{{ image(path="reseau-3/vlan-04.jpg", width="70%" alt="Deux réseau sur deux switchs") }}

Mais alors, il faut deux switchs et deux switchs presque vides en plus.

Une solution serait de réussir à fusionner les deux en un seul.

Ainsi on économiserait de la féraille et de l'électricité pour les faire fonctionnner.

{{ image(path="reseau-3/vlan-06.jpg", width="70%" alt="Deux réseau sur deux switchs") }}

Seulement, nous avons un deuxième obstacle, le bâtiment est vertical.

- le serveur web est à l'étage 2 ainsi qu'un des postes de compta
- le reste des machines à l'étage 1, y compris la passerelle.

{{ image(path="reseau-3/vlan-07.jpg", width="70%" alt="Deux réseau sur deux switchs") }}

On comprend alors ici l'intérêt de fusionner les switchs, on a seulement besoin de 2 switchs au lieu de 4.

Le problèmes est qu'un switch n'est pas très intelligents, il n'isole pas, il met au contraire en relation. 

Si l'on ne fait rien, les deux réseaux vont communiquer. Autrement dit le [flood ARP](/wireguard-1/#broadcast-reseau) atteindra les machines
de la compta et elles seront donc visibles.

L'un de nos buts va être d'empêcher l'ARP de se diffuser anarchiquement dans nos réseaux.

## VLAN

Le termes VLAN ou vlan, est l'acronyme de Virtual Local Area Network, autrement dit un réseau virtuel local.

Un peu comme plusieurs machines virtuelles tournent sur un même PC.

L'idée ici est d'avoir plusieurs réseaux logique sur le même hardware réseau.

Cela permet tout comme la virtualisation de réduire les coûts, cloisonner et avoir plus de flexibilité dans l'usage du matériel physique.

Pour cela il nous faut de nouveaux outils qui vont porter cette virtualisation.

Nous allons commencer par modifier ce qui circule dans le réseau physique.

## Trames identifiées

Avant de commencer cette partie, voici un petit rappel, ceci se nomme une trame ou *frame* en anglais.

Ce sont les données qui transitent physiquement dans les câbles du réseau.

Pour le détails voir la [partie 2](/wireguard-1).

{{ image(path="reseau-3/vlan-08.jpg", width="50%" alt="Une trame réseau") }}

Nous allons introduire une couche supplémentaire qui va nous permettre de rajouter de l'information sur la trame réseau.

{% info() %}
Selon l'implémentation, soit c'est réellement une couche supplémentaire d'encapsulation dans le cadre de Cisco et de l'[ISL](https://fr.wikipedia.org/wiki/Cisco_Inter-Switch_Link), soit c'est un champ supplémentaire que la trame si on est dans le cadre de la norme [IEE 802.1Q](https://fr.wikipedia.org/wiki/IEEE_802.1Q).

Pour des raisons de lisibilté des schémas, je ferai apparaître à part la trame et le tag, mais gardez à l'esprit que l'on reste dans la couche 2 ou de "liaison" du modèle OSI et TCP/IP.
{% end %}

{{ image(path="reseau-3/vlan-10.jpg", width="60%" alt="Une trame réseau") }}

Cette information s'appelle un `identifiant`, mais tout le monde appellera ça un `tag`, donc nous allons faire pareil. 😀

Nous nous retrouvons donc dans l'ordre avec:

- le tag : que nous allons détailler
- la trame : qui contient les informations pour le transfert dans le réseau 
    - addresse MAC de destination et de source 
    - type de trame : ARP, IP, ...
- la couche réseau: 
    - addresse IP
    - ARP
- le transport : permet la gestion des erreurs et l'acheminement des données 
 - protocole : UDP, TCP, RTP, ...
 - ports
- les données

Maintenant que les présentations sont faites avec le nouveau venu dans la famille, nous pouvons détailler comment ce tag est créé et propagé.

## Port tagging

Pour cela, nous devons revenir au matériel réseau.

Voici un switch, vu de plus près.

{{ image(path="reseau-3/vlan-12.jpg", width="60%" alt="Un switch et ses ports") }}

Il est composé de ports. 

Jusqu'à présent le port d'un switch servait uniquement d'interface entre le switch et le réseau.

Le protocole ARP permettant d'identifier les hôtes connectés aux ports, mais pas de rôle actif à remarquer.

Nous allons remédier à cela. 😉

Pour cela nous avons besoin de rajouter des informations supplémentaire à nos ports.

{{ image(path="reseau-3/port-01.jpg", width="60%" alt="Un port taggué") }}

Ces informations sont au nombre de 2:
- Le PVID : Port VLAN IDentifier
- Les actions associées à un TAG (elles peuvent être multiples)

La principale idée du PVID est que seule les ports qui ont le même PVID sont susceptibles de communiquer entre eux.

{{ image(path="reseau-3/port-02.jpg", width="80%" alt="Un port taggué") }}

&nbsp;

{% caution() %}
Les ports ne sont pas électriquement reliés, c'est un artifice de vulgarisation.
{% end %}

L'autre parti des informations supplémentaires sont les actions.

Elles sont au nombre de 2:
- untagged
- tagged

Nous allons voir en détail leurs fonctionnements.

### Untagged

La première action `untagged` ou "non marquée", a un nom qui est réellement trompeur.

Pour comprendre le fonctionnement, il faut dissocier ce qui se passe lorsqu'une trame rentre ou sort du switch.

Commençons par le sens de rentrée de la trame dans le switch.

{{ image(path="reseau-3/vlan-13.jpg", width="70%" alt="Un port untagged en entré") }}

Le dessin se lit de la droite vers la gauche, la trame vient du réseau et se dirige vers le switch.

Elle pénètre le switch par un port qui possède le `PVID=2`.

Ce port a pour action `untagged`, qui contrairement à son nom, fait l'exacte inverse, elle vient ajouter un tag qui vaut la valeur de `2`, valeur héritée du `PVID`.

Ainsi la trame initialement sans tag sort du port côté switch avec un $tag=2$.

Dans le sens contraire, le `untagged` reprend sa signification et vient retirer le tag.

{{ image(path="reseau-3/vlan-14.jpg", width="70%" alt="Un port untagged en sortie") }}

La trame précédemment marquée sort donc du switch non-marquée.

### Tagged

Le deuxième type d'action est le `tagged`, qui à l'inverse signifie "marquée".

Il faut voir l'action tagged comme une non-action.

Un port *tagged* acceptera une trame déjà identifiée. Et la propagera avec son tag.

{{ image(path="reseau-3/vlan-22.jpg", width="70%" alt="Un port tagged en entrée") }}

Mais refusera les trames non-identifiées

{{ image(path="reseau-3/port-04.jpg", width="70%" alt="Refus d'une trame non tagguée") }}

Ou identifié par un tag non reconnu, ici le tag de la trame est 3 alors que le tag accepté est 2.

{{ image(path="reseau-3/vlan-24.jpg", width="70%" alt="Refus d'une trame mal-tagguée") }}

Il est à remarquer ici que le PVID n'est pas pris en compte.

Dans le sens contraire, c'est pareil, la trame tagguée le reste en sortie de port.

{{ image(path="reseau-3/vlan-23.jpg", width="70%" alt="Sortie d'un port taggué") }}

La question maintenant est: est-ce que l'on peut faire sortir des trames qui sont tagguées différement ?

Petit problème, je vous est dit précédement que seul les ports possédant un PVID commun peuvent discuter entre eux.

Or ici le PVID=2 ne peut accepter que des trames identifiées également 2.

La question sous-jacente est donc comment permettre d'accepter des trames tagguées différement ?

{{ image(path="reseau-3/tagged-01.jpg", width="70%" alt="Impossibilité de plusieurs PVID") }}

La réponse est d'avoir virtuellement plus d'un PVID.

{{ image(path="reseau-3/tagged-02.jpg", width="70%" alt="PVID multiple") }}

Ainsi il est possible d'être relié à plus d'un VLAN et de propagé les trames identifiées sur le réseau.

Cette habilité va nous servir par la suite.

{% info() %}
Pourquoi je dis "virtuellement" ?

En pratique, il n'y a qu'un seul PVID par port, celui-ci sert à tagger un trame dans le cas d'un port `untagged`.

Mais on peut conceptuellement dire qu'un port qui aurait plusieurs actions `tagged` possède virtuellement le PVID associé.

Sinon, il pourrait accepter des trames tagguées mais ne pourrait pas les renvoyer avec ces mêmes tags.

{{ image(path="reseau-3/tagged-03.jpg", width="70%" alt="Sortie d'un port taggué") }}

&nbsp;
On aurait alors une communication à sens unique.
{% end %}

Quoi qu'il en soit, un port `tagged` sortira des trames marquées comme son nom l'indique.

## Les types de ports

Le port tagging est un outil qui va nous permettre de construire des rôles spécifique à nos ports.

Avant de voir ces rôles, nous devons introduire le concept de matériel `VLAN aware` et de matériel `VLAN unaware`.

Le mot `aware` outre d'avoir rendu notre plus grand philosophe immortel, veut dire "être conscient de". Dans notre contexte "vlan aware" veut donc dire: "être conscient d'être dans un VLAN".

La plupart des matériels réseaux seront `VLAN unawre`, ils n'ont pas connaissance de l'existence du VLAN et ne savent pas les manipuler.

Dans cette catégorie se trouve les ordinateurs que l'on branche sur un switch.

A comprendre que les trames en entrée et sortie d'un switch qui est connecté à un appareil vlan unawre seront non marquées. 

### Port Access

Les matériels vlan unaware vont être branchés sur un type de ports appelés `ports d'accès` ou *Access Port*.

Pour les constituer nous utilisons l'action `untagged` des ports.

Voici donc nos configurations de ports.

{{ image(path="reseau-3/vlan-15.jpg", width="60%" alt="Configuration d'un port rouge en PVID 4 et d'un bleu en PVID 3 tous les deux en untagged") }}

Nous avons un port "bleu" qui possède un PVID=3 et donc qui appartient de fait au VLAN 3. Ainsi qu'un port "rouge" ayant un PVID=4 et donc dans le VLAN 4.

Une trame ARP arrive par le port le plus à gauche. Elle possède comme auparavant une addresse MAC source.

{{ image(path="reseau-3/vlan-16.jpg", width="80%" alt="Arrivé d'une trame non taggué dans un switch") }}

La trame est décodée et l'association entre l'adresse MAC et le port d'entrée est réalisée.

Mais contrairement à ce qui se passait dans la partie 2, la table n'est pas globale mais associée à un VLAN, dans le cas présent celui du PVID=3 du port "bleu".

La deuxième différence est que la trame se retrouve identifiée avec le tag=3 dans le switch.

{{ image(path="reseau-3/vlan-17.jpg", width="80%" alt="Association du port et de l'adresse MAC") }}

Le switch réalise alors la duplication de la trame ARP non plus sur tous les ports du switch, mais seulement ceux qui sont associés au tag de la trame ici $3$.

{{ image(path="reseau-3/vlan-18.jpg", width="80%" alt="Tagging de la trame") }}

Ainsi seul les ports 2, 4 et 6 qui possède le `PVID=3` transmettent une trame, mais du fait de l'action `untagged` qui leur est appliquée la trame est débarassée de 
son tag avant de rejoindre le réseau.

L'ARP fonctionnant ainsi, seul l'hôte qui est concerné par le paquet répond, et venant généralement d'un hôte vlan unaware, la trame de réponse ARP sera également
non marquée.

Cette trame est alors décodée et la l'adresse MAC de source de la réponse ARP est analysée pour remplir la table de mapping entre les ports et l'adresse MAC.

{{ image(path="reseau-3/vlan-19.jpg", width="80%" alt="Retour du paquet ARP") }}

Le port étant un port d'accès possédant un PVID=3, la trame de réponse ARP est identifiée par le tag=3.

{{ image(path="reseau-3/vlan-20.jpg", width="80%" alt="") }}

Connaissant le port de destination du fait de la précédente identification lors de l'analyse de la requête ARP. La trame restée marquée dans son transit dans le switch, est
acheminée vers le port 1.

Port 1 qui est toujours un port d'accès transfert la trame débarassée de son tag vers l'hôte vlan unaware qui avait fait la requête initiale.

Nous avons donc deux mondes: le réseau où sont connecté les appareils "unaware" et le monde du switch qui est vlan aware.

Pour résumer.

Le principe des ports d'accès est:
- une trame en entrée sans tag se retrouve avec un tag en sortie, la valeur du tag ajouté est le PVID du port
- une trame identifiée perd son tag en sortie

{% caution(header="Point sécurité") %}
Un port untagged refusera une trame déjà tagguée.
&nbsp;
{{ image(path="reseau-3/port-03.jpg", width="80%" alt="Interdiction de re-tagging") }}

Cette précaution est réalisée pour ne pas "re-tagguer" une trame, ce qui peut conduire à des [problèmes de sécurité](https://www.cs.dartmouth.edu/~sergey/netreads/local/l2/bullrl-defcon24.pdf).

{% end %}

Nous venons de régler le premier souci que nous avions: rendre invisible les serveurs du département de comptabilité aux yeux du serveur web.

Si celui-ci est compromis, le pirate lorsqu'il fera la découverte ARP du réseau ne sera pas en mesure de voir le service de comptabilité.

(il y a d'autres moyens et d'autres manières de sécuriser)

{{ image(path="reseau-3/access-01.jpg", width="80%" alt="") }}

Il verra la base de données et la passerelle mais pas les autres machines.

### Port Trunk

Par contre nous n'avons pas agi sur le fait que notre immeuble possède deux étages et donc deux switchs.

{{ image(path="reseau-3/vlan-07.jpg", width="80%" alt="") }}

La solution naïve est d'utiliser deux ports par switchs pour faire transiter les données.

{{ image(path="reseau-3/trunk-01.jpg", width="100%" alt="") }}

Mais on a alors un gaspillage de port et de câble, la virtualisation en prend un coup car on a autant de câble que de VLAN.

Ici, nous avons que 2 VLANs, mais avec 3 VLANs c'est 3 câbles et 6 ports qui partent en fumée.

Il serait plus élégant de tout faire passer par un même câble, ainsi seul deux ports et un câble serait mobilisé quelque soit le nombre de VLANs 
que l'on souhaite faire échanger entre les switchs.

{{ image(path="reseau-3/trunk-02.jpg", width="70%" alt="") }}

Pour cela, nous allons nous servir des ports multiplement taggués.

{{ image(path="reseau-3/trunk-03.jpg", width="100%" alt="") }}

{{ image(path="reseau-3/tagged-02.jpg", width="30%" alt="PVID multiple") }}

Mais on peut grandement simplifier les informations car la liaisons PVID/tagged étant implicite, nous pouvons la remplacer par la 
notation `trunk : X`, X étant l'identifiant du VLAN que l'on souhaite faire transiter.

{{ image(path="reseau-3/vlan-29.jpg", width="30%" alt="") }}

Ceci est appelé un `Trunk port`, dans le cas présent il accepte les VLAN 3 et 4.

On dézoom, notre port trunk sera le 7.

{{ image(path="reseau-3/vlan-28.jpg", width="70%" alt="") }}

Une requête ARP atteint le port d'accès 5 qui possède le PVID=4, la trame est alors dupliquée vers les ports qui sont dans le VLAN 4.

Le port 3 enlève le tag.

Le port 7 par contre, ne fait rien, il transmet tel quel la trame identifié par le tag=4 qui part dans le câble connecté au port 7.

Dans le même temps on enregistre l'adresse MAC et on l'associe au port d'entré.

{{ image(path="reseau-3/vlan-30.jpg", width="70%" alt="") }}

La trame de la requête ARP tagguée part donc en direction du second switch.

{{ image(path="reseau-3/vlan-31.jpg", width="70%" alt="") }}

Elle rentre également par un port 7 qui est lui aussi un port trunk des VLAN 3 et 4.

Le port étant un trunk, la trame conserve son tag et est duppliquée sur tous les port qui ont le PVID=4.

Chacun de ces ports étant des ports d'accès, le tag est alors retiré avant d'être transmis dans le réseau.

{{ image(path="reseau-3/vlan-32.jpg", width="70%" alt="") }}

Là encore, un seul repond.

On enregistre la correspondance de port.

{{ image(path="reseau-3/vlan-33.jpg", width="70%" alt="") }}

Puis on fait transiter vers le port de destination qui s'avère être le 7.

Le port trunk fait alors son office et transmet la trame tagguée sur le câble de liaison.

{{ image(path="reseau-3/vlan-34.jpg", width="100%" alt="") }}

Celle-ci atteint alors l'autre switch qui sait désormais que l'hôte cible de la requête ARP est sur le port 7. Il le note dans sa table.

La réponse ARP sort alors débarassée de son tag vers l'hôte de la requête.

#### VLAN natif

Petit ajout d'information en passant.

Que se passe-t-il si l'on a besoin de faire transiter des trames non-identifiées dans le trunk ?

Ce besoin peut exister dans le cas de vieille infrastructure ou de switch qui ne sont pas vlan aware.

Nous allons réserver un VLAN pour les trames non-identifiées.

Et comme nous sommes dans le cas d'avoir plus d'un PVID. On ne peut pas en dériver la valeur pour le `untagged`.

Nous sommes donc obligé de le spécifier explicitement.

{{ image(path="reseau-3/vlan-25.jpg", width="100%" alt="") }}

Et tout comme la notation tagged/PVID n'est pas adaptée, la notion untagged n'est pas non plus représentative.

Et nous allons l'appeler `native vlan` à la place.

{{ image(path="reseau-3/vlan-26.jpg", width="100%" alt="") }}

Via ce native vlan, les trames non-identifiées en entrée seront tagguées 1 et les trames avec le tag 1 en sortie seront dé-tagguées.

Nous avons donc deux mondes:
- le monde unaware qui manipule les trames non-marquées
- le monde aware qui est capable de manipuler des trames marquées

Et voilà avec ce deuxième type de port trunk on règle également la problématique de l'extension du VLAN tout en conservant une architecture physique qui
ne dépend pas du nombre de VLAN.

## Le voyage d'une trame dans un VLAN

Pour faire une synthèse des outils que nous avons vu, nous analyser ce qui se passe lorsque l'on communique avec le serveur dans le vlan depuis internet.

Ayant maintenant les termes et leur compréhension, nous pouvons annoter le schéma initial.

{{ image(path="reseau-3/vlan-35.jpg", width="100%" alt="") }}

On se retrouve avec notre seveur web et la base de données connectée sur un port d'accès sur le VLAN 3.

Les postes de la comptabilité sont isolés dans le VLAN 4.

Les switchs sont sur un trunk acceptant les VLAN 3 et 4.

La passerelle est également vlan aware et donc possède une interface capable de manipuler et de comprendre les trames possédant des tags. L'interface réseau physique
possède deux interfaces réseau virtuelles, l'une qui tag les trames pour le VLAN 3, l'autre pour le VLAN 4.

Seul l'interface sur le VLAN 3 est routée vers internet.

L'autre partie de la passerelle connectée à un internet n'est pas non plus vlan aware, et donc il n'y aura pas de trames identifiées venant ou partant sur internet.

{% info() %}
Router signifie manipuler la partie "IP" du contenu de la trame, on verra en long en large et en travers le routing dans le prochain article de la série. 
{% end %}

La trame arrive non tagguée depuis internet.

Cette trame réseau encapsule un paquet IP qui possède:
- une addresse IP de destination : l'IP publique de la box internet de l'entreprise
- une addresse IP de source : l'IP publique du client qui fait la requête sur le serveur web

La trame arrive sur la passerelle et désencapsule le paquet.

Une règle de routage sur la box redirige le paquet vers l'interface du VLAN 3.

{{ image(path="reseau-3/vlan-36.jpg", width="100%" alt="") }}

Il se passe alors plusieurs choses qui se déroule dans cet ordre.

- le paquet est encapsulé dans une trame réseau.
- la trame reçoit l'adresse MAC du serveur web comme destination et la MAC de la passerelle comme source grâce à la table ARP de la passerelle.
- la trame est également identifiée avec le tag 3
- la trame sort du port trunk tagguée 3

{{ image(path="reseau-3/vlan-37.jpg", width="70%" alt="") }}

Elle se déplace alors dans le câble qui relie la passerelle au switch taggué 3.

Le port trunk du switch appartenant au vlan 3, la trame est acceptée.

{{ image(path="reseau-3/vlan-38.jpg", width="70%" alt="") }}

Le port acceptant étant un trunk, le tag demeure intact.

La table de correspondance du switch indique que le serveur web est sur le port 3 dans le VLAN 3.

La trame possède le tag 3, elle est donc dirigée vers le port 3.

{{ image(path="reseau-3/vlan-39.jpg", width="70%" alt="") }}

Le port 3 est un trunk du vlan 3, la trame est donc transféré sur le réseau tagguée.

La trame tagguée arrive par le port trunk 2 du vlan 3, elle est acceptée.

La table de correspondance redirige vers le port 1.

Cette fois-ci, le port 1 est un port d'accès, la trame part donc vers le serveur web sans tag.

{{ image(path="reseau-3/vlan-40.jpg", width="70%" alt="") }}

Le serveur web répond par une trame réseau ayant comme destination la passerelle encapsulant un paquet possédant l'IP public du client.

Le port d'accès avec le PVID 3, rajoute le tag 3 à la trame.

La table de correspondance dirige la trame tagguée vers le port 2 qui est un trunk du vlan 3.

La trame sort du switch 2 tagguée.

{{ image(path="reseau-3/vlan-41.jpg", width="70%" alt="") }}

Elle fait le trajet de retour dans le câble qui relie les 2 switchs.

{{ image(path="reseau-3/vlan-42.jpg", width="70%" alt="") }}

Atteint le port 3 qui est un trunk, la trame tagguée traverse le switch.

{{ image(path="reseau-3/vlan-43.jpg", width="70%" alt="") }}

Sort tagguée de l'autre port trunk en direction de la passerelle.

{{ image(path="reseau-3/vlan-44.jpg", width="70%" alt="") }}

La trame tagguée arrive sur l'interface virtuelle du VLAN 3.

Une autre règle de routage basée sur le fait que l'adresse IP de destination est sur internet redirige le paquet vers l'interface réseau côté internet.

La trame non tagguée part sur internet.


{{ image(path="reseau-3/vlan-45.jpg", width="70%" alt="") }}

Bon voyage! 😀

## Qualité de service

On a un petit peu de temps pour nous attarder sur l'autre bienfait de la virtualisation: rajouter de la logique et du service.

Prenons un réseau composé de laptop et de téléphones IP connecté à switch.

Les ports bleus sont dans le vlan 3 et les rouges dans le vlan 4.

{{ image(path="reseau-3/vlan-46.jpg", width="70%" alt="") }}

Chacun de ces matériels sont vlan unaware et sont donc connectés à des ports d'accès et communiquent en trames sans tag.

{{ image(path="reseau-3/vlan-47.jpg", width="70%" alt="") }}

La VOIP ou voix par IP est un type de données qui n'est pas gourmand en trafic, mais celui-ci doit rester extrêment stable durant la conversation.

Or nous sommes dans un monde ou la vraie parallélisation est quelque chose d'extrêment rare, très souvent l'unité de traitement réalise les opération séquentiellement
mais suffisament vite pour donner l'impression que tout se déroule en parrallèle.

Le switch ne fait pas exception, bien qu'il existe des [versions bien énervé qui réalise de la vraie parrallélisation](https://www.fiber-optic-tutorial.com/what-is-a-core-switch.html).

Le notre n'en fait pas.

Ainsi un des laptop se met à réaliser du gros transfert de données, il va oblitérer la capacité de calcul du switch.

La voix va se voir hâchée et la discussion deviendra très difficile.

Comment résoudre cette situation ?

{{ image(path="reseau-3/vlan-48.jpg", width="70%" alt="") }}

La réponse est d'ajouté plus de donnée pour priorisée les trames à traiter.

Nous avons déjà deux VLAN qui découpent le trafic.

Nous pouvons alors utiliser les ports dissociés pour leur donner un nouveau rôle de priorisation.

Lors du tagging, il sera alors possible de rajouter une indication de priorité.

Les trames du port rouge sont désormais 3 fois plus importante que les trames du port bleu.

Autrement dit le VLAN 3 à la priorité sur le VLAN 2.


{{ image(path="reseau-3/vlan-49.jpg", width="40%" alt="") }}

Ainsi cette information de priorité doit être portée par la trame elle-même.

Pour cela le tag ne porte plus uniquement le VLAN ID mais également la priorisation.

{{ image(path="reseau-3/vlan-50.jpg", width="70%" alt="") }}

Pour des raisons de lisibilité je simplifie la notation.

{{ image(path="reseau-3/vlan-51.jpg", width="40%" alt="") }}

Voici le schéma (très simplifié) de principe d'un switch uniquement composé de port d'accès.

De haut en bas, nous avons:
- l'entrée du switch qui vient tagguer la trame et la prioriser
- la répartition en fonction de la priorité
- l'unité de traitement qui réalise la commutation
- le port de sortie qui retire le tag

{{ image(path="reseau-3/vlan-52.jpg", width="70%" alt="") }}

Comme tout à l'heure nous pouvons faire le voyage des trames mais cette fois-ci dans le switch lui-même. 😀

Les trames arrivent non-identifiées du réseau, sont tagguées et priorisées en fonction du port d'entrée.

{{ image(path="reseau-3/vlan-53.jpg", width="100%" alt="") }}

Elles sont alors réparties en fonction de leur priorisation dans ce que l'on nomme des [files de priorités](https://fr.wikipedia.org/wiki/File_de_priorit%C3%A9).

{% info() %}
Dans la réalité ce sont plutôt des [ring-buffers](https://fr.wikipedia.org/wiki/Buffer_circulaire) qui ont la capacité d'"oublier" les trames les plus anciennes pour ne pas engorger le système.
{% end %}

Je représente ici ces files par des réciptient, qui contiennent les trames de même priorité.

{{ image(path="reseau-3/vlan-54.jpg", width="70%" alt="") }}

Le fonctionnement d'une file de priorité est que l'on traite toujours les éléments de plus haute priorité en premier.

{{ image(path="reseau-3/vlan-55.jpg", width="70%" alt="") }}

Puis les priorités les plus faibles par la suite.

{{ image(path="reseau-3/vlan-56.jpg", width="70%" alt="") }}

Ainsi, dès qu'une autre trame de priorité 3 porteur de VOIP arrive sur le switch elle sera traité le plus rapidement possible même si les trames du téléchargement 
continuent d'affluer. 

{{ image(path="reseau-3/vlan-57.jpg", width="70%" alt="") }}

Finalement la commutation est réalisée et les trames sort de leur port d'accès respectif non tagguées.

{{ image(path="reseau-3/vlan-58.jpg", width="70%" alt="") }}

Le VLAN outre d'avoir permis d'isoler les domaine de diffusion entre les laptop et les téléphones IP a également permis de rajouter une information de priorité sur
un trafic par rapport à un autre.

On nomme ceci la QoS (Quality of Service) ou qualité de service en français.

#### En série

Et une dernière choses avant de nous quitter.

Imaginez que vous soyez un peut ric-rac en termes de nombres de port disponible sur un switch et que chacun de vos collaborateurs veut disposer d'un téléphone IP.

Le téléphone peut tout à fait être vlan aware et envoyer des trames tagguées au switch.

On viendra alors connecter le laptop toujours vlan unaware derrière le téléphone.

Le téléphone ne sera alors pas connecté sur un port d'accès mais sur une sorte de trunk (la littérature n'est pas claire sur le nom à donner à ce type de port, j'ai lu parfois port hybride). Un trunk avec un vlan natif ici le 66.

Ainsi le laptop continuera de transmettre des trames non tagguées qui seront relayées par le téléphone.

Toute ces trames voyageront dans le même câble et ne mobiliseront qu'un unique port.

{{ image(path="reseau-3/vlan-59.jpg", width="100%" alt="") }}


## Conclusion

Paradoxalement cet article est celui où j'avais le plus de matière mais où les informations étaient également les plus contradictoires entres-elles.

J'espère ne pas avoir trop simplifier de choses pour le profit de la vulgarisation.

En espérant que vous ayez appris quelques petites choses.

Je vous remercie de votre lecture et je vous dis à la prochaine pour l'article du le routage de paquets. ❤️

Si vous avez des corrections à réaliser vous pouvez le faire via une [merge request](https://gitlab.com/Akanoa/blogv2/-/blob/main/content/wireguard-2.md?ref_type=heads).

[Lien vers la partie 2](/wireguard-1)
